#!/bin/sh
for port in `seq 7001 7006`;
do 
sed -i "s/#{HOST_IP}/${HOST_IP}/g" /home/redis/${port}/conf/redis.conf;
redis-server /home/redis/${port}/conf/redis.conf;
done;
echo yes|redis-cli --cluster create \
"${HOST_IP}":7001 \
"${HOST_IP}":7002 \
"${HOST_IP}":7003 \
"${HOST_IP}":7004 \
"${HOST_IP}":7005 \
"${HOST_IP}":7006 \
--cluster-replicas 1;
