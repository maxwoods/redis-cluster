FROM redis:5.0.4-alpine

ENV HOST_IP 127.0.0.1

ENV PASSWORD redis

#RUN addgroup -S redis && adduser -S -G redis redis
#RUN chown redis:redis /home/redis

RUN mkdir -p /home/redis/6379/conf
RUN mkdir -p /home/redis/6379/data

COPY redis.conf /home/redis/6379/conf/redis.conf
COPY redis-cluster.conf /home/redis/redis-cluster.conf
RUN for port in `seq 7001 7006`; do mkdir -p /home/redis/${port}/conf  && cp /home/redis/redis-cluster.conf /home/redis/${port}/conf/redis.conf && \ 
        sed -i "s/#{PORT}/${port}/g" /home/redis/${port}/conf/redis.conf && \
	mkdir -p /home/redis/${port}/data; done

RUN rm -r /home/redis/redis-cluster.conf;

ADD redis-cluster.sh /home/redis                                                                           
ADD build-redis-cluster.sh /home/redis
RUN chmod +x /home/redis/redis-cluster.sh
RUN chmod +x /home/redis/build-redis-cluster.sh

#RUN addgroup -S redis && adduser -S -G redis redis

#RUN chown redis:redis /home/redis

RUN /home/redis/build-redis-cluster.sh
RUN rm /home/redis/build-redis-cluster.sh

WORKDIR /home/redis

EXPOSE 6379 7001 7002 7003 7004 7005 7006 17001 17002 17003 17004 17005 17006


#CMD ["redis-server","/home/redis/6379/conf/redis.conf"]
ENTRYPOINT ["/home/redis/redis-cluster.sh"]
