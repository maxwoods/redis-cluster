#!/bin/sh
for port in `seq 7001 7006`;
do
sed -i "s/127.0.0.1/${HOST_IP}/g" /home/redis/${port}/conf/redis.conf;
sed -i "s/127.0.0.1/${HOST_IP}/g" /home/redis/${port}/conf/nodes.conf;
echo "masterauth ${PASSWORD}" >> /home/redis/${port}/conf/redis.conf;
echo "requirepass ${PASSWORD}" >> /home/redis/${port}/conf/redis.conf;
redis-server /home/redis/${port}/conf/redis.conf;
done;
echo "masterauth ${PASSWORD}" >> /home/redis/6379/conf/redis.conf;
echo "requirepass ${PASSWORD}" >> /home/redis/6379/conf/redis.conf;
redis-server /home/redis/6379/conf/redis.conf
